 $(document).ready(function () {
     
                  var downloadFile;                
                $("#speech-function").html('');                          
                $("#error-message").hide();                          
                $("#download-speech").hide();
                
                $(document).on('click', '#convert-speech', function(e){                                                                  
                    $("#error-message").hide();                          
                    
                    if($('#text-to-speech-convert-form').valid()) { 
                        e.preventDefault();                             
                        $('#convert-speech').attr("disabled", true);                                                                                
                        text = $('#text').val();
                        lan = $('#lan').val();
                       
                        $.ajax({              
                            url : "{{ route('text-to-speech-convert') }}",
                            type : "get",                                
                            data: {
                               text: text,                               
                               lan: lan                             
                            },                          
                            success:function(data) {                                                                          
                                                                                            
                                $('#convert-speech').attr("disabled", false);  
                                if( data["status"] == 200) {                                                                                                            
                                    downloadFile = data.responseText["download-file"];
                                    $("#download-speech").show(); 
                                    text = $('#text').val('');
                                    lan = $('#lan').prop("selectedIndex", 0);
                                    $("#speech-function").html('<audio controls><source src="'+ data.responseText["play-url"] +'" type="audio/mpeg"> Your browser does not support the audio element.</audio> ');
                                }   
                                else {
                                    $("#error-message").html(data["responseText"]).show(); 
                                }                               
                            },               
                            error:function(data){ 
                                $('#convert-speech').attr("disabled", false);                                
                                $("#error-message").html(data.responseJSON["message"]).show();                         
                                console.log(data);                               
                            }
                            
                        });   
                    }                                                                           
                });
     
     
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
    
   $('.dropdown-toggle').on('click', function(event) {
       
       _href = event.target.href;
       
       if(_href.indexOf('#') > 0) {
       
            _id = _href.substr(_href.indexOf('#'));
            
             $(_id).toggle(500);
            

       }
   }); 
});