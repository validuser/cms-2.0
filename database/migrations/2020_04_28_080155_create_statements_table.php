<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('statements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('categories_id');
            $table->text('description');
            $table->string('title')->nullable();
            $table->string('page', 100)->nullable();
            $table->boolean('active')->default(0);
            $table->string('head_title', 100)->nullable();
            $table->string('meta_description', 200)->nullable();
            $table->string('meta_keywords', 100)->nullable();
            $table->string('tts_audio_path', 100)->nullable();
            $table->bigInteger('created_by_users_id');
            $table->bigInteger('updated_by_users_id')->nullable();            
            # $table->timestamps(); not nullable
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();
       
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
