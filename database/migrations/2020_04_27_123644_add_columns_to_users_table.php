<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->string('first_name', 20)->nullable()->after('name');
            $table->string('surname', 30)->nullable()->after('first_name');
            $table->string('phone', 10)->nullable()->after('surname');
            $table->string('personal_identity_number', 10)->nullable()->after('phone');
            $table->boolean('is_admin')->default(0)->after('email');
            $table->boolean('active')->default(0)->after('is_admin');
            $table->string('department', 50)->nullable()->after('active');
            $table->string('avatar_path', 100)->nullable()->after('department');
            $table->dateTime('logged_at')->nullable()->after('updated_at');
            $table->dateTime('password_changed_at')->nullable()->after('logged_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        
            $columns = array('first_name', 'surname', 'phone', 'personal_identity_number', 'is_admin', 'active', 'department', 'logged_at', 'password_changed_at');

            foreach($columns as $column) {
                
                $table->dropColumn($column);
            }
        });
    }
}
