<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->integer('amount')->unsigned();
            $table->bigInteger('statements_id')->unsigned();
            $table->integer('categories_id')->nullable();
            $table->timestamp('created_at');
#            $table->dropTimestamps();
            
 #           $table->dropColumn('updated_at');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
