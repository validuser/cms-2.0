<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');                        
            $table->string('name', 100);
            $table->boolean('active')->default(0);
            $table->string('link', 100)->nullable();
            # $table->timestamps(); not nullable
            $table->timestamp('created_at');
            $table->timestamp('updated_at')->nullable();                        
            $table->bigInteger('created_by_users_id');
            $table->bigInteger('updated_by_users_id')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
