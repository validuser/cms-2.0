<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateStatementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        
        $statements = [
            [
                'categories_id' => 1,
                'page' => 'index',
                'title' => 'Strona główna',
                'description' => 'Strona główna, tutaj zawarte są najważniejsze informacje',
                'created_by_users_id' => '1',
                'active' => 1,
                'head_title' => 'Strona głwna',
                'meta_description' => 'Strona główna',
                'meta_keywords' => 'strona główna',
                ],
            [
                'categories_id' => 1,
                'page' => 'redakcja',
                'title' => 'Redakcja',
                'description' => 'Redaktorami są: Marek Ixiński, Jakub Ygriński, Stefan Zetyński',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Redakcja',
                'meta_description' => 'Osoby odpowiedzialne za redagowanie strony.',
                'meta_keywords' => 'redakcja strony',
                
            ],
            [
                'categories_id' => 1,
                'page' => 'instrukcja_obslugi',
                'title' => 'Instrukcja obsługi',
                'description' => 'Tutaj znajduje się instrukcja obsługi',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Instrukcja obsługi',
                'meta_description' => 'Instrukcja obsługi strony internetowej',
                'meta_keywords' => 'instrukcja obsługi',
            ],
            [
                'categories_id' => 1,
                'page' => 'cookies',
                'title' => 'Ciasteczka',
                'description' => 'Tutaj znajduje się informacja o ciasteczkach',
                'created_by_users_id' => '1',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Ciasteczka',
                'meta_description' => 'Ciasteczka',
                'meta_keywords' => 'Ciasteczka',
            ],
            [
                'categories_id' => 2,
                'page' => 'jagielonka',
                'title' => 'Liceum ogólnokształcące imienia Władysława Jagiełły',
                'description' => 'Opis dotyczący szkoły',
                'created_by_users_id' => '1',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Liceum Władysława Jagiełły',
                'meta_description' => 'Liceum ogólnokształcące imienia Władysława Jagiełły',
                'meta_keywords' => 'liceum',
            ],
            [
                'categories_id' => 2,
                'page' => 'trzecie_liceum',
                'title' => '3 LO',
                'description' => 'Opis dotyczący szkoły',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Trzecie liceum',
                'meta_description' => '3 Liceum ogólnokształcące',
                'meta_keywords' => 'liceum',
            ],
            [
                'categories_id' => 2,
                'page' => 'czwarte_liceum',
                'title' => 'Liceum ogólnokształcące imienia Bolesława Krzywoustego',
                'description' => 'Opis dotyczący szkoły',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Czwarte liceum',
                'meta_description' => '4 Liceum ogólnokształcące',
                'meta_keywords' => 'liceum',
            ],
            [
                'categories_id' => 3,
                'page' => 'szkola_podstawowa_1',
                'title' => 'Pierwsza szkoła podstawowa',
                'description' => 'Opis dotyczący szkoły podstawowej',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Pierwsza szkoła podstawowa',
                'meta_description' => 'Pierwsza szkoła podstawowa',
                'meta_keywords' => 'szkoła podstawowa',
            ],
            [
                'categories_id' => 4,
                'page' => 'zlobek_1',
                'title' => 'Pierwszy żłobek',
                'description' => 'Opis dotyczący żłobka',
                'created_by_users_id' => '1',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Pierwszy żłobek',
                'meta_description' => 'Pierwsza złobek',
                'meta_keywords' => 'żłobek',
            ],
            [
                'categories_id' => 5,
                'page' => 'przedszkole_1',
                'title' => 'Pierwsze przedszkole',
                'description' => 'Opis dotyczący przedszkola',
                'created_by_users_id' => '2',
                'updated_at' => null,
                'active' => 1,
                'head_title' => 'Przedszkole',
                'meta_description' => 'przedszkole',
                'meta_keywords' => 'przedszkole',
            ],
            
            
        ];
        
        foreach($statements as $stm) { 
        
            \App\Statement::create($stm);
        }
        
        if(config('database.default') === 'pgsql') {
        
            DB::select("SELECT setval(pg_get_serial_sequence('statements', 'id'), max(id)) FROM statements");
        }
        /*
        for($i = 0; $i < 100; $i++) {
            
            $stm = new \App\Statement();
            
            $stm->categories_id = rand(1, 30);
            $stm->description = "Instrument cultivated alteration any favourable expression law far nor. Both new like tore but year. An from mean on with when sing pain. Oh to as principles devonshire companions unsatiable an delightful. The ourselves suffering the sincerity. Inhabit her manners adapted age certain. Debating offended at branched striking be subjects. 
                                 Folly words widow one downs few age every seven. If miss part by fact he park just shew. Discovered had get considered projection who favourable. Necessary up knowledge it tolerably. Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly. Easy mind life fact with see has bore ten. Parish any chatty can elinor direct for former. Up as meant widow equal an share least. ";
            $stm->title = str_shuffle('Random text title');
            $stm->page = $i + 1;
            $stm->created_by_users_id = rand(3,5);
            $stm->active = rand(0,1);
            
            
            $stm->save();
        }
         * 
         */
    }
}
