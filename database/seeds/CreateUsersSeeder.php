<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\User;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run() {
         
        $user = [

            [

               'name'=>'admin',
                
               'first_name' => 'Zenon',
                
               'surname' => 'Xiński',

               'email'=>'admin@gmail.com',

                'is_admin'=>'1',

               'password'=> bcrypt('haslo123'),

            ],

            [

               'name'=>'user',

               'first_name' => 'Mariusz',
                
                'surname' => 'Ygriński',
                
               'email'=>'user@gmail.com',

                'is_admin'=>'0',

               'password'=> bcrypt('haslo123'),

            ],

        ];

  

        foreach ($user as $key => $value) {

            User::create($value);

        }
        
        if(config('database.default') === 'pgsql') {
        
            DB::select("SELECT setval(pg_get_serial_sequence('users', 'id'), max(id)) FROM users");
        }
        
        /*
        for($i = 0; $i < 97; $i++) {
            
            $user = new User();
            
            $randomString = substr(str_shuffle('bastiantestqueue'), 0, 10);
            
            $user->name = $randomString;
            $user->email = $randomString.'@gmail.com';
            $user->first_name = $randomString; 
            $user->surname = $randomString;
            $user->is_admin = 0;
            $user->password = bcrypt(rand(0, 10000));
            
            $user->save();
        }
         * 
         */
    }
}
