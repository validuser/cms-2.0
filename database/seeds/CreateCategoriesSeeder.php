<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         
        $categories = [
            
            [
                'id'  => 1,
                'name' => 'Brak',
                'created_by_users_id' => 1,
                'updated_at' => null,
                'active' => 1,
            ],
            
            [
                'id' => 2,
                'name' => 'Szkoły średnie',
                'link' => 'szkoly_srednie',
                'created_by_users_id' => 1,
                'updated_at' => null,
                'active' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Szkoły podstawowe',
                'link' => 'szkoly_podstawowe',
                'created_by_users_id' => 1,
                'updated_at' => null,
                'active' => 1,
            ],
            [
                'id' => 4,
                'name' => 'Żłobki',
                'link' => 'zlobki',
                'created_by_users_id' => 1,
                'updated_at' => null,
                'active' => 1,
            ],
            [
                'id' => 5,
                'name' => 'Przedszkola',
                'link' => 'przedszkola',
                'created_by_users_id' => 1,
                'updated_at' => null,
                'active' => 1,
            ]
        ];
        
       for($i = 0; $i < count($categories); $i++) {
                    
            \App\Category::create($categories[$i]);
       }
       
        if(config('database.default') === 'pgsql') {
            
            DB::select("SELECT setval(pg_get_serial_sequence('categories', 'id'), max(id)) FROM categories");
        }
       
       /*
       for($i = 0; $i < 30; $i++) {

           \App\Category::create([
                                    'name' => 'test '.$i, 
                                    'created_by_users_id' => 3,
                                    'updated_by_users_id' => 4
                                 ]);
       }
       */
    }
}
