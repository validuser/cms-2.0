<?php

namespace App;

class BaseModel extends \Illuminate\Database\Eloquent\Model {
  
    public function getColumns() {
        
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
