<?php

namespace App;


class Entry extends BaseModel
{
    public $table = 'entries';
    
    protected $fillable = ['created_at', 'amount', 'statements_id', 'categories_id'];
    
    public $timestamps = false;
    
    public function statement() {
        
        return $this->hasOne('\App\Statement', 'id', 'statements_id');
    }
    
    public function category() {
        
        return $this->hasOne('\App\Category', 'id', 'categories_id');
    }
    
}
