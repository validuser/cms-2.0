<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
Use Image;
Use App\Photo;
use Intervention\Image\Exception\NotReadableException;
 
# https://w3path.com/laravel-6-ajax-image-upload-using-image-intervention-pack/ 

class ImageController extends Controller
{
    private $path;
    private $pathThumbNail;
    
    private $pathUrl;
    private $pathThumbNailUrl;
 
    public function __construct() {
        
        $this->path = base_path('public/uploads/');;
        $this->pathThumbNail = base_path('public/uploads/thumbnails/');
        
        $this->pathUrl = url('uploads');
        $this->pathThumbNailUrl = url('uploads/thumbnails');
    }
    
    public function index() {
        
        return view('image')->with('pathUrl', $this->pathUrl)->with('pathThumbNailUrl', $this->pathThumbNailUrl);
    }

    public function save(Request $request) {
      
        request()->validate([
             'photo_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
     
        if ($files = $request->file('photo_name')) {

           // for save original image
           $ImageUpload = Image::make($files);
           $ImageUpload->save($this->path.time().$files->getClientOriginalName());


           // for save thumnail image
           $ImageUpload->resize(250,125);
           $ImageUpload = $ImageUpload->save($this->pathThumbNail.time().$files->getClientOriginalName());

           $photo = new Photo();
           $photo->photo_name = time().$files->getClientOriginalName();
           $photo->users_id = $request->user()->id;
           $photo->save();
        }

        $image = Photo::latest()->first(['photo_name']);
        
        return Response()->json($image);
        
       }
}