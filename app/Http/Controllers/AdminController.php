<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Validator;

use Illuminate\Support\Facades\Artisan;

use \app\User;

use App\Http\Controllers\TextToSpeechController;

class AdminController extends Controller {

    public function __construct() {
        
        $this->middleware('is_admin');
    }
    
    public function index() {
            
        return view('admin/home');
    }
    
    private function getQueryFromFormStatements(Request $request, $paginate = 0) {
        
        $stmt = \App\Statement::where('statements.id', '>', 0);
            
        if($request->input('title')) {

            $stmt->where('statements.title', 'like', '%'.trim($request->input('title')).'%');
        }

        if($request->input('id')) {
            
            $stmt->where('statements.id', (int)$request->input('id'));
        }
        
        if($request->input('categories_id')) {
            
            $stmt->where('statements.categories_id', $request->input('categories_id'));
        }
                
        if($request->input('created_by')) {
            
            $stmt->where('statements.created_by_users_id', $request->input('created_by'));            
        }

        if($request->input('active') != null && ($request->input('active') == 0 || $request->input('active') == 1)) {
            
            $stmt->where('statements.active', $request->input('active'));
        }

        if($request->input('submit') == null) { // sortowanie mozna przekazywac w pdfie, ale nie w nowym wyszukiwaniu
        
            if($request->input('sort_category')) {

                $stmt->join('categories', 'categories.id', 'statements.categories_id')->orderBy('categories.name', ($request->input('sort_category') == 1)? 'ASC' : 'DESC');
            }
                
            if($request->input('sort_creator')) {
                
                $stmt->join('users', 'users.id', 'statements.created_by_users_id')->orderBy('users.name', ($request->input('sort_creator') == 1)? 'ASC' : 'DESC');
            }
            
            if($request->input('sort_editor')) {
                
                $stmt->join('users', 'users.id', 'statements.updated_by_users_id')->orderBy('users.name', ($request->input('sort_editor') == 1)? 'ASC' : 'DESC');
            }
            
            if($request->input('sort_link')) {
                
                $stmt->orderBy('statements.id', ($request->input('sort_link') == 1)? 'ASC' : 'DESC');
            }
           
            if($request->input('sort_title')) {
             
                $stmt->orderBy('statements.title',  ($request->input('sort_title') == 1)? 'ASC' : 'DESC');
            }
         
            if($request->input('sort_created_at')) {
                
                $stmt->orderBy('statements.created_at', ($request->input('sort_created_at') == 1)? 'ASC' : 'DESC');
            }
            
            if($request->input('sort_updated_at')) {
                
                $stmt->orderBy('statements.updated_at', ($request->input('sort_updated_at') == 1)? 'ASC' : 'DESC');
            }     
        }
        
        #echo $stmt->toSql();
        
        #die();
        
        
        return ($paginate)? $stmt->paginate($paginate) : $stmt->get();
    }
    
    private function createStatementsPdf(Request $request) {
        
        $pdf = \PDF::loadView('pdf.statements', ['statements' => $this->getQueryFromFormStatements($request)]);
        
        return $pdf->download('statements.pdf');
    }
    
    public function getStatements(Request $request) {
        
        if($request->input('pdf_create')) {
            
            return $this->createStatementsPdf($request);
        }
        
        $categories = \App\Category::where('active', 1)->orderBy('id', 'ASC')->get();
        $ids = \App\Statement::orderBy('id', 'ASC')->get();

        # ->where('users.active', '1')
        $users = \App\User::join('statements', 'statements.created_by_users_id', 'users.id')->select('users.*')->groupBy('users.id')->get();
        
        return view('admin/statements')->with('statements', $this->getQueryFromFormStatements($request, 8))
                                       ->with('categories', $categories)
                                       ->with('createdByUsers', $users)
                                       ->with('ids', $ids);
    }
    
    public function saveStatement(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:5|max:50',
            'description' => 'required:string',
            'categories_id' => 'required|integer',
            'active' => 'required|boolean',
            'head_title' => 'string|min:5|max:100',
            'meta_description' => 'string|min:5|max:200',
            'meta_keywords' => 'string|min:5|max:100',
            'id' => 'integer'
        ]);

        $add = $request->input('id') == null;
        
        if($validator->fails()) {
            
            return redirect('admin/statement'.(($add)? '/'.$request->input('id') : ''))->withErrors($validator)->withInput();
        }
        else {

            if($add) {
                
                $stm = new \App\Statement();
                
                $stm->title = $request->input('title');
                $stm->description = $request->input('description');
                $stm->categories_id = $request->input('categories_id');
                $stm->active = $request->input('active');
                /* $stm->page = (int)(\App\Statement::max('page')) + 1; */
                $stm->page = ($request->input('page') != null)? $request->input('page') : '';
                $stm->created_by_users_id = Auth::user()->id;
                $stm->head_title = $request->input('head_title');
                $stm->meta_description = $request->input('meta_description');
                $stm->meta_keywords = $request->input('meta_keywords');
                
                # opis do tlumaczenia musi byc krotszy niz 500 znakow
                if(!empty($request->input('description')) && !is_null($path = TextToSpeechController::TextToSpeechConvert($request))) {

                    $stm->tts_audio_path = $path;
                }
                $stm->save();
                
                return redirect('admin/statements')->withSuccess(['success' => 'Strona została dodana']);
                
            }
            else { # update
                
                if(($stm = \App\Statement::find($request->input('id'))) != null) {
                    
                    $stm->title = $request->input('title');
                    $stm->description = $request->input('description');
                    $stm->categories_id = $request->input('categories_id');
                    $stm->active = $request->input('active');
                    $stm->page = ($request->input('page') != null)? $request->input('page') : '';
                    $stm->updated_by_users_id = Auth::user()->id;
                    $stm->head_title = $request->input('head_title');
                    $stm->meta_description = $request->input('meta_description');
                    $stm->meta_keywords = $request->input('meta_keywords');
                    
                    if(!is_null($path = TextToSpeechController::TextToSpeechConvert($request))) {

                        $stm->tts_audio_path = $path;
                    }
                    $stm->save();
                    
                    
                    $stm->update();
                    
                    return redirect('admin/statement/'.$request->input('id'))->withSuccess(['success' => 'Strona została zaktualizowana']);
                }
                else 
                    return redirect('admin/statements')->withErrors(['error' => 'Strona nie została znaleziona']);
            }            
            
            return view('admin/statements')->withSuccess(['success' => 'Strona została '.(($add)? 'dodana' : 'zaktualizowana')]);
        } 
            
    }
    
    public function setActiveStatement($id, $active) {
        
        if($id != null && ($stm = \App\Statement::find($id)) != null) {
            
            $stm->active = ($active)? 1:0;
            $stm->save();
            
            return redirect()->back()->withSuccess(['success' => 'Strona o identyfikatorze '.$id.' została '.($active? 'włączona' : 'wyłączona')]);            
        }
        else return redirect()->back()->withErrors(['error' => 'Brak strony o podanym identyfikatorze: '.$id]);
    }
    
    public function getStatement($id = null) {
        
        $stm = null;
        
        if($id == null) {
            
            $stm = new \App\Statement();
        }
        else if(($obj = \App\Statement::find($id)) != null){
            
            $stm = $obj;
        }
        else return redirect('/admin/statements')->back()->withErrors(['error' => 'Brak strony o podanym identyfikatorze: '.$id]);


        return view('admin/statement')->with('stm', $stm)->with('categories', \App\Category::get());
    }
    

    private function getQueryFromFormCategories(Request $request, $paginate = 0) {
        
        $stmt = \App\Category::where('id', '>', 0);
            
        if($request->input('name')) {

            $stmt->where('name', 'like', '%'.trim($request->input('name')).'%');
        }
        
        if($request->input('createdby')) {
            
            $stmt->where('created_by_users_id', $request->input('createdby'));
        }

        if($request->input('active') != null && ($request->input('active') == 0 || $request->input('active') == 1)) {
            
            $stmt->where('active', $request->input('active'));
        }
        
                
        if($request->input('submit') == null) { // sortowanie mozna przekazywac w pdfie, ale nie w nowym wyszukiwaniu
            
            $params = ['name' => 'sort_name', 'created_at' => 'sort_create_date', 'created_by_users_id' => 'sort_created_by', 'updated_at' => 'sort_update_date', 'updated_by_users_id' => 'sort_updated_by'];
            
            foreach($params as $name => $param) {
                
                if($request->input($param)) {
                                        
                    $direction = ($request->input($param) == 1)? 'ASC' : 'DESC';

                    $stmt->orderBy($name, $direction);
                }
            }            
        }
        
        return ($paginate)? $stmt->paginate($paginate) : $stmt->get();
    }
    
    private function createCategoriesPdf(Request $request) {
        
        $pdf = \PDF::loadView('pdf.categories', ['categories' => $this->getQueryFromFormCategories($request)]);
        
        return $pdf->download('categories.pdf');
    }
    
    public function getCategories(Request $request) {
        
        if($request->input('pdf_create')) {
            
            return $this->createCategoriesPdf($request);
        }
        
        $authors = DB::table('categories')->join('users', 'users.id', 'categories.created_by_users_id',)->select('users.id', 'users.name')->groupBy('users.id')->get(); # toSql();
        
        return view('admin/categories')->with('categories', $this->getQueryFromFormCategories($request, 8))->with('authors', $authors);
    }
    
    public function saveCategory(Request $request) {
        
        if((($name = $request->input('name')) != null) && ($request->input('link') != null)) {

            $userId = Auth::user()->id;
            
            if($request->input('id') != null && ($category = \App\Category::find($request->input('id'))) != null) { //update category
                            
                $category->name = $name;
                $category->link = ($request->input('link') != null)? $request->input('link') : '';
                $category->updated_by_users_id = $userId;
                $category->active = ($request->input('active'))? 1:0;
                $category->save();
                
                return redirect('/admin/categories')->withSuccess(['success' => 'Kategoria została zaktualizowana']);
            }
            else if($request->input('id') == null) { // new category
            
                if(\App\Category::where('name', 'like', '%'.trim($name).'%')->count() == 0) {
                    
                    $newCategory = new \App\Category();                    
                    $newCategory->name = $name;
                    $newCategory->created_by_users_id = $userId;
                    $newCategory->updated_by_users_id = $userId;
                    $newCategory->updated_at = null;
                    $newCategory->link = $request->input('link');
                    $newCategory->active = ($request->input('active'))? 1:0;                                        
                    $newCategory->save();

                    return redirect('/admin/categories')->withSuccess(['success' => 'Nowa kategoria została dodana']);
                }
                else return redirect()->back()->withErrors(['error' => 'Kategoria o podanej nazwie już istnieje']);            
            }
            else return redirect()->back()->withErrors(['errors' => 'Brak kategorii o podanym identyfikatorze']);
        }
        else return redirect()->back()->withErrors(['error' => 'Brak nazwy lub linku dla kategorii']);
    }
    
    public function getCategory($id = null) {
        
        if($id == null) { // new category
            
            return view('admin/category')->with('category', new \App\Category());
        }
        else if($id != null && ($category = \App\Category::find($id)) != null) { // edit category
                        
            return view('admin/category')->with('category', \App\Category::find($id));
        }
        else
            return redirect()->back()->withErrors(['error' => 'Brak kategori o podanym identyfikatorze']);
    }
    
    public function setActiveCategory($id, $active) {
        
        if($id != null && ($category = \App\Category::find($id)) != null) {

            $category->active = ($active)? 1:0;
            $category->save();
            
            return redirect()->back()->withSuccess(['success' => 'Kategoria została '.(($active)? 'włączona' : 'wyłączona')]);            
        }
        else return redirect()->back()->withErrors(['error' => 'Brak kategorii o podanym identyfikatorze']);
    }
    
    
    private function getQueryFromForm(Request $request, $paginate = 0) {
        
        $stmt = \App\User::where('id', '>', 0);
        
        if($request->input('name')) {

            $stmt->where('name', 'like', '%'.trim($request->input('name')).'%');
        }

        if($request->input('email')) {

            $stmt->where('email', 'like', '%'.trim($request->input('email')).'%');
        }

        if($request->input('first_name')) {

            $stmt->where('first_name', 'like', '%'.trim($request->input('first_name')).'%');
        }

        if($request->input('surname')) {

            $stmt->where('surname', 'like', '%'.trim($request->input('surname')).'%');
        }
        
                
        if($request->input('submit') == null) { // sortowanie mozna przekazywac w pdfie, ale nie w nowym wyszukiwaniu
            
            if($request->input('sort_name')) {

                $direction = ($request->input('sort_name') == 1)? 'ASC' : 'DESC';

                $stmt->orderBy('name', $direction);
            }

            if($request->input('sort_email')) {

                $direction = ($request->input('sort_email') == 1)? 'ASC' : 'DESC';

                $stmt->orderBy('email', $direction);
            }

            if($request->input('sort_first_name')) {

                $direction = ($request->input('sort_first_name') == 1)? 'ASC' : 'DESC';

                $stmt->orderBy('first_name', $direction);
            }

            if($request->input('sort_surname')) {

                $direction = ($request->input('sort_surname') == 1)? 'ASC' : 'DESC';

                $stmt->orderBy('surname', $direction);
            }
        }
        
        return ($paginate)? $stmt->paginate($paginate) : $stmt->get();
    }
    
    private function createUsersPdf(Request $request) {
        
        $pdf = \PDF::loadView('pdf.users', ['users' => $this->getQueryFromForm($request)]);
        
        return $pdf->download('users.pdf');
    }
    
    public function adminUsers(Request $request) {
        
        if($request->input('pdf_create')) {
            
            return $this->createUsersPdf($request);
        }
        
        return view('admin/users')->with('users', $this->getQueryFromForm($request, 8))->with('authUser', Auth::user());
    }
    
    public function setAdminAccess($id, $access = 1) {
        
        if($id != null && (($user = \App\User::find($id)) != null)) {
            
            $user->is_admin = ($access)? 1:0;
            $user->save();
            
            return redirect()->back()->withSuccess(['success' => 'Użytkownikowi '.$user->name.' zostały '.(($access)? 'nadane' : 'odebrane').' uprawnienia administratora']);
        }
        else return redirect()->back()->withErrors(['errors' => 'Nieprawidłowy identyfikator użytkownika']);  
    }
    
    public function setActive($id, $active = 0) {
        
        if($id != null && (($user = \App\User::find($id)) != null)) {
            
            $user->active = ($active)? 1:0;
            $user->save();
            
            return redirect()->back()->withSuccess(['success' => 'Użytkownik '.$user->name.' '.(($active)? 'uzyskał dostęp do systemu' : ' nie ma już dostępu do systemu')]);
        }
        else return redirect()->back()->withErrors(['errors' => 'Nieprawidłowy identyfikator użytkownika']);
    }
    
    public function changePassword($id) {
        
        if($id != null && (($user = \App\User::find($id)) != null)) {
            
            // wyslanie wiadomosci do uzytkownika, zawierajacej specjalnego linka do zmiany hasla
            // tworzenie wiadomosci email
            
            return redirect()->back()->withSuccess(['success' => 'Wysłano mail z linkiem do zmiany hasła dla użytkownika '.$user->name]);            
        }
        else return redirect()->back()->withErrors(['error' => 'Nieprawidłowy identyfikator użytkownika']);
    }   
    
    public function account($id = 0) {
        
        $authUser = Auth::user();
        
        if($authUser->is_admin && $id != 0) {
            
            $user = \App\User::find($id);
                
           if($user == null) return redirect('/admin/users')->withErrors(['errors' => 'Użytkownik, którego dane próbowano zmienić, nie istnieje!']);        
        }
        else $user = $authUser;
        
                
        $avatar = url('/uploads/'.$user->id).'/'.$user->avatar_path;
                
        return view('account/account')->with('avatar', $avatar)->with('user', $user);
    }
    
    public function reset() {
        
        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
        
        die('Baza została odświeżona');
    }
    
    public function getSchema($name) {
        
        $prefix = 'App\\';
        
        $path = $prefix.$name;
        
        if(class_exists($path)) {
                                
            $table = new $path();

            return view('admin/db/schema')->with('table', $table);
        }
        else echo 'brak klasy';
    }
    
    public function getTables() {
        
        $tablesNames = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        
        return view('admin/db/tables')->with('tables', $tablesNames);
    }
}
