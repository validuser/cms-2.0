<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class EntryController extends Controller
{
    
    public static function add($pageId = 0) {
               
        if(($entry = EntryController::get($pageId)) != null) {
            
            if(!EntryController::update($entry)) {
                
                Log::info('Entry table failed to update: '.$pageId);
            }
        }
        else {
            
            if(!EntryController::addEntry($pageId)) {
                
                Log::info('Entry table failed to add: '.$pageId);
            }
        }        
    }
    
    public static function addCategory($categoryId = 0) {
        
        if(($entry = EntryController::getByCategory($categoryId)) != null) {
            
            if(!EntryController::update($entry)) {
                
                Log::info('Entry table failed to update: '.$categoryId);
            }
        }
        else {
            
            if(!EntryController::addEntryCategory($categoryId)) {
                
                Log::info('Entry table failed to add: '.$categoryId);
            }
        }  
    }
    
    private static function getByCategory($categoryId) {
        
        return \App\Entry::where('categories_id', $categoryId)
                         ->whereDay('created_at', '=', date('d'))
                         ->whereMonth('created_at', '=', date('m'))
                         ->whereYear('created_at', '=', date('Y'))
                         ->first();
    }
    
    private static function get($pageId) {

        return \App\Entry::where('statements_id', $pageId)
                         ->whereDay('created_at', '=', date('d'))
                         ->whereMonth('created_at', '=', date('m'))
                         ->whereYear('created_at', '=', date('Y'))
                         ->first();
    }
    
    private static function addEntry($pageId) {
        
        $entry = new \App\Entry();
 
        $entry->amount = 1;
        $entry->statements_id = $pageId;
        $entry->created_at = Carbon::now()->toDateTimeString();
        
        return $entry->save();        
    }
    
    private static function update($entry) {
        
        $entry->amount += 1;
        return $entry->save();
    }
    
    private static function addEntryCategory($categoryId) {
        
        $entry = new \App\Entry();
 
        $entry->amount = 1;
        $entry->statements_id = 0;
        $entry->categories_id = $categoryId;
        $entry->created_at = Carbon::now()->toDateTimeString();
        
        return $entry->save();        
    }
    
    private static function updateCategory($entry) {
        
        $entry->amount += 1;
        return $entry->save();
    }
    
    # select s.title, sum(e.amount) from entries e join statements s on e.statements_id = s.id where s.active =1 group by statements_id order by 2 Desc 
    public static function getMostViewed() {
        
        # return \App\Entry::join('statements', 'statements.id', 'entries.statements_id')->where('statements.active', 1)->select('statements.title', DB::raw('SUM(entries.amount) as amount'))->groupBy('statements.id')->orderBy('amount', 'DeSC')->get();
        
        
        $statements = \App\Entry::join('statements', 'statements.id', 'entries.statements_id')->where('statements.active', 1)->select('statements.title', DB::raw('SUM(entries.amount) as amount'))->groupBy('statements.id');

        $categories = \App\Entry::join('categories', 'categories.id', 'entries.categories_id')->where('categories.active', 1)->select('categories.name as title', DB::raw('SUM(entries.amount) as amount'))->groupBy('categories.id');
        
        return $statements->union($categories)->orderBy('amount', 'DeSC')->get();
    }
    
    public static function getSum() {
        
        return \App\Entry::select(DB::raw('sum(amount) as amount'))->first()->amount;
    }
    
    # SELECT dayofweek(created_at), sum(amount) From entries GROUP BY 1 ORDER BY 1 DESC 
    public static function getDaysViewed() {
        
        if(config('database.default') == 'mysql')
            return \App\Entry::select(DB::raw('DAYOFWEEK(created_at) as day'), DB::raw('COUNT(amount) as amount'))->groupBy('day')->orderBy('day', 'ASC')->get();
        else
            return null;            
    }
    
    # SELECT YEAR(created_at), SUM(amount) FROM `entries` GROUP BY 1 ORDER BY 1 DESC 
    public static function getYearsViewed() {
        
        if(config('database.default') == 'mysql')
            return \App\Entry::select(DB::raw('YEAR(created_at) as year'), DB::raw('SUM(amount) as amount'))->groupBy('year')->orderBy('year', 'DESC')->get();
        else
            return null;
    }
}
