<?php

namespace App\Http\Controllers;

use App\Helpers\VoiceRSS;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TextToSpeechController extends Controller
{        
    public static function TextToSpeechConvert(Request $request)
    {            
        $description = strip_tags($request->description);
        $description = (strlen($description) > 500)?  substr($description, 0, 500) : $description;

            try {

                    $tts = new VoiceRSS;
                    $voice = $tts->speech([
                        'key' => env('VOICE_RSS_API_KEY'),
                        'hl' =>  'pl-pl',
                        'src' => $description,
                        'r' => '0',
                        'c' => 'mp3',
                        'f' => '44khz_16bit_stereo',
                        'ssml' => 'false',
                        'b64' => 'false'
                    ]);	

                    
                    $filename = Str::uuid().'.mp3';
                    if( empty($voice["error"]) ) {		

                        $rawData = $voice["response"];	

                        if (!File::exists(storage_path('app/public/tts')))
                        {
                                Storage::makeDirectory(public_path('storage/tts'));
                        }

                        Storage::disk('tts')->put($filename, $rawData);
                        $speechFilelink =  asset('storage/tts/'.$filename);							   		                 
                        $urls["play-url"] = $speechFilelink;		   	
                        $urls["download-file"] = $filename;	

                        # $data = array('status' => 200, 'responseText' => $urls);

                        # return response()->json($data);

                        return $filename; # $speechFilelink;
                    }


                    #$data = array('status' => 400, 'responseText' => "Please try again 1");
                    #return response()->json($data);     
                return null;

            } 
            catch (SitemapParserException $e) {
                #$data = array('status' => 400, 'responseText' => $e->getMessage());
                # return response()->json($data);
                return null;
            }               
    }

}