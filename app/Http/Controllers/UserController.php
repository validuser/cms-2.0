<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EntryController;

class UserController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $categories, $languageAvailable;
    
    public function __construct()
    {
        # $this->middleware('auth');
        
        #phpinfo();
        #die();
        
        $this->categories = \App\Category::where('active', 1)->where('name', '!=', 'Brak')->orderBy('id', 'ASC')->get();
        
        $this->languageAvailable = ['pl', 'en'];
        
        if(!empty(\Cookie::get('language')) && ($cookie = \Crypt::decrypt(\Cookie::get('language'), false)) 
                && in_array($cookie, $this->languageAvailable)) {
             
            \App::setLocale($cookie);
        }
    }

    /* old version not used*/
    public function getPage($categoryId, $pageId) {
        
        if(!is_null($page = \App\Statement::find($pageId))) {
        
            return view('page')->with('categories', $this->categories)
                               ->with('page', $page);   
        }
        else 
            return redirect()->back()->withErrors(['error' => 'Brak strony o podanym id '.$page]);
    }
    
    /*all pages link from news from main page*/
    public function getStatements() {

        return view('statements')->with('categories', $this->categories)
                                 ->with('statements', \App\Statement::where('active', 1)->orderBy('id', 'DeSC')->paginate(5))
                                 ->with('bread', 'Wszystkie strony')
                                 ->with('head_title', 'Wszystkie strony')
                                 ->with('meta_description', '')
                                 ->with('meta_keywords', '');
    }
    
    public function getSearch(Request $request) {
        
        if(($text = $request->input('text')) != null &&  strlen($text) > 3) {

            $stm = \App\Statement::where('active', 1)->where(function($x) use ($text) {
                
                $x->where('title', 'LIKe', '%'.$text.'%')->orWhere('description', 'LIKe', '%'.$text.'%');
            })->orderBy('id', 'DeSC')->paginate(5);

            return view('statements')->with('categories', $this->categories)
                                     ->with('statements', $stm)->with('search', $text)
                                     ->with('bread', 'Wyniki wyszukiwania')
                                     ->with('head_title', 'Wyniki wyszukiwania')
                                     ->with('meta_description', '')
                                     ->with('meta_keywords', '');
        }
        else 
            return redirect()->back()->withErrors(['error' => 'Szukana wyrażenie jest zbyt krótkie']);
    }
    
    public function getMostViewed() {
        
        
        
        return view('entries')->with('categories', $this->categories)
                              ->with('entries', EntryController::getMostViewed())
                              ->with('days', EntryController::getDaysViewed())
                              ->with('years', EntryController::getYearsViewed())
                              ->with('sum', EntryController::getSum())
                              ->with('bread', 'Statystyki serwisu')
                              ->with('head_title', 'Statystyki serwisu')
                              ->with('meta_description', '')
                              ->with('meta_keywords', '');
    }
    
    // category/title
    // category
    // title
    // main_page
    
    public function getPageByName($categoryNameOrPage = '', $pageName = '') {
        
        # echo asset('storage/tts/0ddf0d69-38f1-473f-a087-b70f8d2525a9.mp3');	
        #echo storage_path('0ddf0d69-38f1-473f-a087-b70f8d2525a9.mp3');
        
        #print_r(\Storage::disk('tts')->path(''));
        # die();
        
        if(empty($categoryNameOrPage) && empty($pageName)) { # main page

            if(!is_null($page = \App\Statement::where('page', 'index')->where('active', 1)->first())) {
            
                EntryController::add($page->id);
                
                return view('page')->with('categories', $this->categories)
                                   ->with('page', $page)
                                   ->with('categoryBreadCrumbs', '')
                                   ->with('news', \App\Statement::where('active', 1)->orderBy('id', 'DeSC')->limit(3)->get());
            }
            else 
                abort(404, 'Brak strony o podanym adresie!');
        }        
        else if(!empty($categoryNameOrPage) && !empty($pageName)) {
            
            if(!is_null($category = \App\Category::where('link', $categoryNameOrPage)->where('active', 1)->first()) && !is_null($page = \App\Statement::where('page', $pageName)->where('active', 1)->first())) {
                
                EntryController::add($page->id);
                
                return view('page')->with('categories', $this->categories)
                               ->with('page', $page)
                               ->with('categoryBreadCrumbs', $category);
            }
            else 
                abort(404, 'Brak strony o podanym adresie');
        }
        else if(!empty($categoryNameOrPage)) {
            
            if(!is_null($category = \App\Category::where('link', $categoryNameOrPage)->where('active', 1)->first())) {
                
                EntryController::addCategory($category->id);
                
                return view('category')->with('categories', $this->categories)
                                       ->with('categoryBreadCrumbs', $category)
                                       ->with('category', $category);
            }
            else if(!is_null($page = \App\Statement::where('page', $categoryNameOrPage)->where('active', 1)->first()) && $page->category->name == 'Brak') {
                
                EntryController::add($page->id);
                
                return view('page')->with('categories', $this->categories)
                               ->with('page', $page)
                               ->with('categoryBreadCrumbs', '');
            }
            else
                abort(404, 'Brak strony o podanym adresie');
        }
        else 
                abort(404, 'Brak strony o podanym adresie');
    }
    
    public function setCookie() {        
        
        return redirect()->back()->cookie('cookies_permission', '1',60 *24 *30 * 12);
    }
    
    public function setCookieWithFontSize($size = 1.1) {

        if(!($size >= 1 && $size <= 5)) {
            
            $size = 1;
        }
        
        return redirect()->back()->cookie('font_size', $size, 60 *24 *30 * 12);
    }
    
    public function changeLanguage($lang = 'pl') {
        
        if (in_array($lang, $this->languageAvailable)) {
            
            return redirect()->back()->cookie('language', $lang, 60 *24 *30 * 12);
        }
        else return abort(404, 'Brak podanego jezyka');
        
    }
    
  
}