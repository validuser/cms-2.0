<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PhotoController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Photo;

class UserAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    
    public function index() {
        
        return view('home');
    }
    
    public function account() {
        
        $user = Auth::user();        
                
        $avatar = url('/uploads/'.$user->id).'/'.$user->avatar_path;
                
        return view('account/account')->with('avatar', $avatar)->with('user', $user);
    }
    
    public function password() {
        
        $user = Auth::user();
        
        return view('account/password')->with('user', $user);
    }
    
    public function saveAccount(Request $request) {
        
        return UserAccountController::updateAccount($request);
    }
    
    # change password or avatar
    public function savePassword(Request $request) {
        
        return UserAccountController::updatePassword($request);
    }
    
    private static function updateAccount(Request $request) {
          
        if($request->user() != null && $userId = $request->user()->id) {
        
            $id = ($request->user()->is_admin && $request->input('id') != 0)? $request->input('id') : $userId;  
            
            $user = \App\User::find($id);
            
            if($user != null) {
            
                $validator = Validator::make($request->all(),[
                    'first_name' => 'required|string|max:20|min:3', 
                    'surname' => 'required|string|max:30|min:3',
                    'personal_identity_number' => 'size:10', # |integer||unique:users,personal_identity_number
                    'phone' => 'size:9', # |integer
                    'department' => 'string|max:50',
                    ]);

                # validation which cannot be checked earlier

                if($request->input('personal_identity_number') != null && !is_numeric($request->input('personal_identity_number'))) {

                    $validator->getMessageBag()->add('personal_identity_number', 'Pole pesel musi zawierać wyłącznie cyfry.');
                }

                if($request->input('phone') != null && !is_numeric($request->input('phone'))) {

                    $validator->getMessageBag()->add('phone', 'Pole telefon musi zawierać wyłącznie cyfry, bez spacji, bez myślników.');
                }

                /*
                # returns errors but not activate fails method       
                print_r($validator->errors()->all());

                if($validator->fails()) {
                    echo 'fail';
                }

                */

                if(count($validator->errors()->all()) == 0 && !$validator->fails()) {

                    $user->first_name = $request->input('first_name');
                    $user->surname = $request->input('surname');
                    $user->personal_identity_number = $request->input('personal_identity_number');
                    $user->phone = $request->input('phone');
                    $user->department = $request->input('department');

                    if(($file = $request->file('avatar')) != null) {

                        $user->avatar_path = PhotoController::resizeRequest($request);
                    }

                    $user->save();

                    if($request->user()->is_admin)
                        return redirect('/admin/user/'.$id)->withSucces(['success' => 'Zmiana danych powiodła się.']);
                    else
                        return redirect('/home/account')->withSucces(['success' => 'Zmiana danych powiodła się.']);
                }
                else {

                    if($request->user()->is_admin)
                        return redirect('/admin/user/'.$id)->withErrors($validator)->withInput();
                    else
                        return redirect('/home/account')->withErrors($validator)->withInput();
                }             
            }
            else 
                if($request->user()->is_admin) 
                    return redirect('/admin/users')->withErrors(['errors' => 'Użytkownik, którego dane próbowano zmienić, nie istnieje!']);            
                else
                    return redirect('/home/account')->withErrors(['errors' => 'Nastąpił niespodziewany błąd!']);
        }
        else return redirect('/home')->withErrors(['error' => 'Nie jesteś zalogowany!']);
    }
    
    private static function updatePassword(Request $request) {       
        
        if($request->user() != null && $id = $request->user()->id) {
            
            $user = \App\User::find($id);
            
            if($request->input('new_password') != null || $request->input('repeat_new_password') != null) {
                
                if(($new_password = $request->input('new_password')) != null && ($repeat_new_password = $request->input('repeat_new_password')) != null) {
                    
                    if($new_password === $repeat_new_password) {
                        
                        if(($old_password = $request->input('old_password')) != null) {
                            
                            if($old_password !== $new_password) {
                                
                                if(Hash::check($request->input('old_password'), $request->user()->password)) {    
 
                                    if(strlen($new_password) < 8) {
                                         
                                        return redirect('/home/password')->withErrors(['error', 'Hasło musi zawierać przynajmniej 8 znaków!']);
                                    }
                                    elseif(!preg_match("#[0-9]+#",$new_password)) {
                                        return redirect('/home/password')->withErrors(['error' => 'Hasło musi zawierać przynajmniej 1 cyfrę!']);
                                    }
                                    elseif(!preg_match("#[A-Z]+#",$new_password)) {
                                        return redirect('/home/password')->withErrors(['error' => 'Hasło musi zawierać przynajmniej 1 dużą literę!']);
                                    }
                                    elseif(!preg_match("#[a-z]+#",$new_password)) {
                                        return redirect('/home/password')->withErrors(['error' => 'Hasło musi zawierać przynajmniej 1 małą literę!']);
                                    }
                                    else {
                                    
                                        $user->password = Hash::make($new_password);
                                    }
                                }
                                else {
                                    
                                    return redirect('/home/password')->withErrors(['error' => 'Stare hasło nie jest prawidłowe!']);
                                }  
                            }
                            else {
                                
                                return redirect('/home/password')->withErrors(['error' => 'Stare hasło nie różni się od nowego!']);
                            }
                        }
                        else {
                            
                            return redirect('/home/password')->withErrors(['error' => 'Brak starego hasła!']);
                        }
                    }
                    else {
                         
                        return redirect('/home/password')->withErrors(['error' => 'Hasła nie zgadzają się!']);
                    }
                }
                else {
                    
                    return redirect('/home/password')->withErrors(['error' => 'Brakuje jednego z haseł!']);
                }
            }
            
            /*
            if(($file = $request->file('avatar')) != null) {
                
                $user->avatar_path = PhotoController::resizeRequest($request);
                    
            }
            */
            $user->save();
            
            return redirect('/home/password')->withSuccess(['status' => 'Hasło zostało zaktualizowane']);
                    
        }
        else return redirect('/home')->withErrors(['error' => 'Nie jesteś zalogowany!']);
    }
}
