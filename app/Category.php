<?php

namespace App;

class Category extends BaseModel
{
    public $table = 'categories';
    
    protected $fillable = ['name', 'link', 'created_by_users_id', 'updated_by_users_id', 'active'];

    
    public function statements() {
        
        return $this->hasMany('\App\Statement', 'categories_id', 'id');
    }
    
    public function createdBy() {
        
        return $this->hasOne('\App\User', 'id', 'created_by_users_id');
    }
    
    public function updatedBy() {
     
        return $this->hasOne('\App\User', 'id', 'updated_by_users_id');
    }
}
