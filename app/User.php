<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;
    public $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'first_name', 'surname', 'department'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function categoriesCreatedBy() {
       
        return $this->hasMany('\App\Categories', 'created_by_users_id', 'id');
    }
    
    public function categoriesUpdatedBy() {
        
        return $this->hasMany('\App\Categories', 'updated_by_users_id', 'id');
    }
    

    
    public function getPath() {
        
        return url('/uploads/'.$this->id).'/'.$this->avatar_path;
    }
    
    public function exist() {
        
        return file_exists(public_path().'/uploads/'.$this->id.'/'.$this->avatar_path);
    }
}
