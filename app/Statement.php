<?php

namespace App;

class Statement extends BaseModel
{
    public $table = 'statements';
    
    protected $fillable = ['categories_id', 'description', 'title', 'created_by_users_id', 'updated_by_users_id', 'active', 'page', 'head_title', 'meta_keywords', 'meta_description', 'tts_audio_path'];
    
    public function category() {
        
        return $this->hasOne('\App\Category', 'id', 'categories_id');
    }
    
    public function createdBy() {
        
        return $this->hasOne('\App\User', 'id', 'created_by_users_id');
    }
    
    public function updatedBy() {
        
        return $this->hasOne('\App\User', 'id', 'updated_by_users_id');
    }
    
    public function link() {
        
        return url('/').(!empty($this->category->link)? '/'.$this->category->link : '').'/'.$this->page;
    }
    
    public function shortDescription() {
        
        $description = strip_tags($this->description);
        
        return substr($description, 0, 120).((strlen($description))? ' ...' : '');
    }
}
