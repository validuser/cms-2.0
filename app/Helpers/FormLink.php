<?php

namespace App\Helpers;


class FormLink {
    
    protected static $sortParams = [];
    protected static $searchParams = [];


    public static function getLink(array $add, array $delete = array()) {
        
        $self = get_called_class();
        
        $localCopy = $self::$sortParams;
        $localCopy['submit'] = 'submit';
        
        return $self::getLinkFromRequest($add, $localCopy);                
    }
 
    
     public static function getInputFromQuery() {
        
        if(($query = request()->query()) != null) {
            
            foreach((get_called_class())::$sortParams as $name) {
                
                if(isset($query[$name])) {
                    
                    echo '<input type="hidden" name="'.$name.'" value="'.$query[$name].'" />';
                }
            }
        }
    }
    
    public static function getParamsFromQuery() {
        
        $self = get_called_class();
        
        $sortParams = array();
        $search = array_merge($self::$sortParams, $self::$searchParams);        
        
        if(($query = request()->query()) != null) {
            
            foreach($search as $name) {
                
                if(isset($query[$name])) {
                    
                    $sortParams[$name] = $query[$name];                    
                }
            }
        }
        
        return $sortParams;        
    }
    
    private static function getLinkFromRequest(array $add, array $delete) {
        
        $url = url()->current();
        $query = request()->query();
        
        if($query != null && count($delete) > 0) {
        
            foreach($delete as $param) {

                unset($query[$param]);
            }           
        }
        
        if(count($add) > 0) {
            
            foreach($add as $param => $value) {
                $query[$param] = $value;
            }
        }
            
        
        return (count($query) == 0)? $url : $url.'?'.http_build_query($query);   
    }
    
    /* only example */
    private static function removeParams(array $params = [])
    {
        $url = url()->current(); // get the base URL - everything to the left of the "?"
        $query = request()->query(); // get the query parameters (what follows the "?")

        foreach($params as $param) {
            unset($query[$param]); // loop through the array of parameters we wish to remove and unset the parameter from the query array
        }

        # print_r($query);
        # die($url.' -- '.http_build_query($query));
        
        
        return $query ? $url . '?' . http_build_query($query) : $url; // rebuild the URL with the remaining parameters, don't append the "?" if there aren't any query parameters left
    }
    
    /* only example */
    private static function addParams(array $params = [])
    {
        $query = array_merge(
            request()->query(),
            $params
        ); // merge the existing query parameters with the ones we want to add

        return url()->current() . '?' . http_build_query($query); // rebuild the URL with the new parameters array
    }
}
