<?php

namespace App\Helpers;

class StatementsFormLink extends FormLink {
    
    protected static $sortParams = ['sort_link', 'sort_title', 'sort_category', 'sort_created_at', 'sort_creator', 'sort_updated_at', 'sort_editor'];
    protected static $searchParams = ['id', 'title', 'categories_id', 'created_by', 'active'];
}

/* config/app.php - add class to get visibility on whole project*/