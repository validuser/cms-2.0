<?php


namespace App\Helpers;


class CategoriesFormLink extends FormLink{
   
    protected static $sortParams = ['sort_name', 'sort_create_date', 'sort_created_by', 'sort_update_date', 'sort_updated_by'];
    protected static $searchParams = ['name', 'createby', 'active'];
}
