<?php

namespace App\Helpers;

class UsersFormLink extends FormLink {

    protected static $sortParams = ['sort_name', 'sort_email', 'sort_first_name', 'sort_surname'];
    protected static $searchParams = ['name', 'email', 'first_name', 'surname'];  
}
