<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'login' => 'Zaloguj',
    'register' => 'Zarejestruj',
    'email' => 'email',
    'password' => 'hasło',
    'remember' => 'Zapamietaj mnie',
    'forget'  => 'Zapomniałeś hasła?',

];
