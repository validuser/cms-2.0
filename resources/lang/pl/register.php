<?php

return [
    'register' => 'Rejestracja',
    'name' => 'Nazwa',
    'email' => 'Adres email',
    'password' => 'Hasło',
    'confirm_password' => 'Powtórz hasło',
];