@extends('admin.admin')

@section('main')

<div class="card-header">Dodaj kategorię</div>

<div class="card-body">
    <form method="POST" action="/admin/category/save">

        <div class="form-group">
            <label for="category_name">Nazwa</label>
            <input id="category_name" name="name" type="text" value="{{$category->name}}" class="form-control" />
        </div>
        
        <div class="form-group">
            <label for="category_link">Link</label>
            <input id="category_link" name="link" value="{{$category->link}}" class="form-control" />
        </div>
        
        <div class="form-group">
            <label for="category_active">Aktywny</label>
            <select id="category_active" name="active" class="form-control">
                <option value="0" @if(!$category->active)selected="selected" @endif>nie</option>
                <option value="1" @if($category->active) selected="selected" @endif>tak</option>
            </select>
        </div>
        
        <input type="submit" value="Zapisz" class="form-control btn-primary" />
        <input type="hidden" name="id" value="{{$category->id}}" />
        
        @csrf
    </form>
</div>
    
@endsection