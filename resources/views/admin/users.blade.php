@extends('admin.admin')

@section('main')

<div class="card">

        <form id="search-users" method="GET" action="/admin/users/">
            
            <div class="card-header">
            
                <div class="form-row">
                    <div class="col-md-9">Wyszukaj użytkownika</div>

                    <div class="col-md-1">
                        <input name="submit" type="submit" value="szukaj" class="btn btn-primary btn-sm w-100" />                    
                    </div>

                    <div class="col-md-1">
                        <input name="pdf_create" type="submit" value="pdf" class="btn btn-primary btn-sm w-100" />
                    </div>
                    
                     <div class="col-md-1">
                        <a href="/admin/users" class="btn btn-primary btn-sm w-100">Wyczyść</a>
                    </div>
                </div>
            </div>
            
            <div class="card-body form-group">
            <div class="form-row">
                <div class="col-md-3">
                    <label for="user_name">Nazwa</label>
                    <input id="user_name" name="name" type="text" value="{{Request::get('name')}}" placeholder="Nazwa" class="form-control"/>
                </div>
        
                <div class="col-md-3">
                     <label for="user_email">Email</label>
                     <input id="user_email" name="email" type="text" value="{{Request::get('email')}}" placeholder="Email" class="form-control" />
                </div>
       
                <div class="col-md-3">
                     <label for="user_first_name">Imię</label>
                     <input id="user_first_name" name="first_name" type="text" value="{{Request::get('first_name')}}" placeholder="Imię" class="form-control" />
                </div>
                
                <div class="col-md-3">
                    <label for="user_surname">Nazwisko</label>
                    <input id="user_surname" name="surname" type="text" value="{{Request::get('surname')}}" placeholder="Nazwisko" class="form-control" />
                </div>
            </div>

            
            </div>
            
            {{UsersFormLink::getInputFromQuery()}}
        </form>      
    
   
    <div class="card-header">Lista użytkowników</div>

    <div class="card-body">
            {{$users->appends(UsersFormLink::getParamsFromQuery())->links()}}
        <table class="table-responsive table table-sm table-striped table-bordered table-hover">
            <thead class="">
                <tr>
                    <th scope="col" class="col-sm-2">nazwa
     
                        <a href="{{UsersFormLink::getLink(['sort_name' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{UsersFormLink::getLink(['sort_name' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                       
                    </th>
                    <th scope="col" class="col-sm-2">email
                        
                        <a href="{{UsersFormLink::getLink(['sort_email' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{UsersFormLink::getLink(['sort_email' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th scope="col"class="col-sm-1">administrator</th>
                    <th class="col-sm-2">imie
                        <a href="{{UsersFormLink::getLink(['sort_first_name' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{UsersFormLink::getLink(['sort_first_name' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">nazwisko
                        <a href="{{UsersFormLink::getLink(['sort_surname' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{UsersFormLink::getLink(['sort_surname' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-1">data logowania</th>
                    <th class="col-sm-1">zmiana hasła</th>
                    <th class="col-sm-1">aktywny</th>
                </tr>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td><a href="/admin/user/{{$user->id}}">{{$user->name}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if($user->is_admin)
                            tak<br/>
                            @if($user->id != $authUser->id)
                                <a href="/admin/access/{{$user->id}}/0" onclick="return confirm('Czy na pewno?');">Odbierz</a>
                            @endif
                        @else
                            <a href="/admin/access/{{$user->id}}" onclick="return confirm('Czy na pewno?');">Przyznaj</a>
                        @endif                                    
                    </td>
                    <td>{{$user->first_name}}</td>
                    <td>{{$user->surname}}</td>
                    <td>@if($user->logged_at != null){{$user->logged_at}} @else nigdy @endif</td>
                    <td>
                        @if($user->password_changed_at != null){{$user->password_changed_at}} @else nigdy @endif<br/>
                        <a href="/admin/user/password/{{$user->id}}" onclick="return confirm('Czy na pewno?');">Wyślij link</a><br/>
                    </td>
                    
                    <td>
                        @if($user->active)
                            <a href="/admin/user/active/{{$user->id}}" onclick="return confirm('Czy na pewno?')">tak</a>
                        @else
                            <a href="/admin/user/active/{{$user->id}}/1" onclick="return confirm('Czy na pewno?')">nie</a>
                        @endif
                    </td>
                   
                </tr>
            @endforeach
        </table>
        {{$users->appends(UsersFormLink::getParamsFromQuery())->links()}}
        
        
       
    </div>

</div>

@endsection