@extends('admin/admin')

@section('main')

 <script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js')}}"></script>

  <script type="text/javascript">
    tinyMCE.init({
      mode : "textareas",
      language: 'pl',
      contextmenu: "link image imagetools table spellchecker",
      toolbar: 'fontselect fontsizeselect',
      font_formats: 'Arial=arial,helvetica,sans-serif;',
      fontsize_formats: '0.8rem 0.9rem 1.0rem 1.1rem 1.2rem',
      height: 200,
     
    });
    
</script>

<div class="card">
    
    <div class="card-header">
        @if(Request::get('id') != null)edytuj @else dodaj @endif stronę
    </div>
    
    <div class="card-body">
        <form method="POST" action="/admin/statement/save">
            
            <div class="form-group">
                <label for="stm_title">Tytuł</label>
                <input id="stm_title" name="title" type="text" value="{{$stm->title}}" class="form-control" />
            </div>
            
            <div class="form-group">
                <label for="stm_description">Opis</label>
                <textarea id="stm_description" name="description" class="form-control">{{$stm->description}}</textarea>
            </div>
            
            <div class="form-group">
                <label for="stm_category">Kategoria</label>
                <select id="stm_category" name="categories_id" class="form-control">
                    <option value="-1" @if($stm->categories_id == null) selected="selected" @endif>brak</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" @if($category->id == $stm->categories_id) selected="selected" @endif>
                            {{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="stm_active">Aktywny</label>
                <select id="stm_active" name="active" class="form-control">
                    @foreach(Config::get('global.active') as $key => $value)
                        <option value="{{$key}}" @if($key == $stm->active) selected="selected" @endif>{{$value}}</option>
                    @endforeach
                </select>                
            </div>
            
            <div class="form-group">
                <label for="page">Strona</label>
                <input type="text" id="page" name="page" value="{{$stm->page}}" class="form-control" />
            </div>
            
            <div class="form-group">
                <label for="head_title">Tytuł strony</label>
                <input id="head_title" name="head_title" type="text" value="{{$stm->head_title}}"  class="form-control" />
            </div>
            
            <div class="form-group">
                <label for="meta_keywords">Słowa kluczowe</label>
                <textarea id="meta_keywords" name="meta_keywords" class="form-control">{{$stm->meta_keywords}}</textarea>
            </div>
            
            <div class="form-group">
                <label for="meta_description">Opis</label>
                <textarea id="meta_description" name="meta_description" class="form-control">{{$stm->meta_description}}</textarea>
            </div>
            
            <div class="text-right">
                <input type="submit" name="submit" value="zapisz" class="btn btn-primary" />
            </div>
            @if($stm->id)
            <input type="hidden" name="id" value="{{$stm->id}}" />
            @endif
            
            @csrf
        </form>
    </div>
</div>

@endsection