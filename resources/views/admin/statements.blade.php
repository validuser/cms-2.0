@extends('admin.admin')

@section('main')

<div class="card">

    <form method="GET" action="/admin/statements">
    
        <div class="card-header form-row">
            <div class="col-md-9">
                Wyszukaj
            </div>
           
            <div class="col-md-1">
                <input type="submit" name="submit" value="szukaj" class="btn btn-primary btn-rounded btn-sm w-100" />
            </div>
            
            <div class="col-md-1">
                <input type="submit" name="pdf_create" value="pdf" class="btn btn-primary btn-rounded btn-sm w-100" />
            </div>
            
            <div class="col-md-1">
                <a href="/admin/statements" class="btn btn-primary btn-rounded btn-sm w-100">Wyczyść</a>
            </div>
        </div>

        <div class="card-body form-group form-row">
            
            <div class="col-md-1">
                <label for="s_id">Id</label>
                <select id="s_id" name="id" class="form-control">
                    <option value="0" @if(Request::get('id') == null) selected="selected"@endif>-</option>
                    @foreach($ids as $obj)
                    <option value="{{$obj->id}}" @if(Request::get('id') == $obj->id) selected="selected" @endif>{{$obj->id}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="col-md-3">
                <label for="s_title">Tytuł</label>
                <input id="s_title" type="text" name="title" value="{{Request::get('title')}}" placeholder="Tytuł" class="form-control" />
            </div>
            
            
            <div class="col-md-3">
                <label for="s_categories_id">Kategoria</label>
                <select id="s_categories_id" name="categories_id" class="form-control">
                    
                    <option value="0" @if(Request::get('id') == null) @endif>Wszystkie</option>
                    
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" @if($category->id == Request::get('categories_id')) selected="selected" @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="col-md-3">
                <label for="s_users_add">Dodał</label>
                <select id="s_users_add" name="created_by" class="form-control">
                    <option value="0" @if(Request::get('created_by') == null) selected="selected" @endif>Wszyscy</option>
                    @foreach($createdByUsers as $user)
                        <option value="{{$user->id}}" @if(Request::get('created_by') == $user->id) selected="selected" @endif>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            
            
            
            <div class="col-md-2">
                <label for="s_active">Aktywny</label>
                
                <select id="s_active" name="active" class="form-control">
                    <option value="-1" @if(Request::get('active') == null) selected="selected" @endif>-</option>
                    
                    @foreach(Config::get('global.active') as $key => $value)
                        <option value="{{$key}}" @if((Request::get('active') != null) && ($key == Request::get('active'))) selected="selected" @endif>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    
        @csrf
        
        {{StatementsFormLink::getInputFromQuery()}}
    </form>
    
    <div class="card-header">Lista stron</div>

    <div class="card-body">


        {{$statements->appends(StatementsFormLink::getParamsFromQuery())->links()}}
        
        <table class="table-responsive table table-sm table-striped table-bordered table-hover">
            <tr>
                <th scope="col">id
                    <a href="{{StatementsFormLink::getLink(['sort_link' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_link' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">tytuł
                    <a href="{{StatementsFormLink::getLink(['sort_title' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_title' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">kategoria
                    <a href="{{StatementsFormLink::getLink(['sort_category' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_category' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">data dodania
                    <a href="{{StatementsFormLink::getLink(['sort_created_at' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_created_at' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">dodał
                    <a href="{{StatementsFormLink::getLink(['sort_creator' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_creator' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">data edycji
                    <a href="{{StatementsFormLink::getLink(['sort_updated_at' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_updated_at' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">edytował
                    <a href="{{StatementsFormLink::getLink(['sort_editor' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                    <a href="{{StatementsFormLink::getLink(['sort_editor' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                </th>
                <th scope="col">aktywny</th>
            </tr>

            @foreach($statements as $s)
            <tr>
                <td>
                    <a href="/{{$s->category->link}}/{{$s->page}}" target="_blank">{{$s->id}}</a>
                </td>
                <td><a href="/admin/statement/{{$s->id}}">{{$s->title}}</a></td>
                <td><a href="/admin/category/{{$s->categories_id}}">{{$s->category->name}}</a></td>
                <td>{{$s->created_at}}</td>
                <td>{{$s->createdBy->name}}</td>
                <td>{{$s->updated_at}}</td>
                <td>
                    @if($s->updated_by_users_id != null)
                        {{$s->updatedBy->name}}
                    @else
                        brak
                    @endif
                </td>
                <td>@if($s->active) <a href="/admin/statement/{{$s->id}}/access/0">tak</a> @else <a href="/admin/statement/{{$s->id}}/access/1">nie</a> @endif</td>
            </tr>
            @endforeach

        </table>
        {{$statements->appends(StatementsFormLink::getParamsFromQuery())->links()}}
        
        <p class="text-right"><a href="/admin/statement" class="btn btn-primary">Dodaj strone &raquo;</a></p>
    </div>
    
</div>

@endsection
