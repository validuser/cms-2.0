@extends('admin.admin')

@section('main')

<div class="card-header">{{$table->table}}</div>

<div class="card-body">
    
    @foreach($table->getColumns() as $column)
        {{$column}}<br/>
    @endforeach
</div>
    
@endsection