@extends('admin.admin')

@section('main')

<div class="card">

    <form id="search-users" method="GET" action="/admin/categories/">
            
    
    <div class="card-header form-row">

        <div class="col-md-9">Wyszukaj kategorie</div>

        <div class="col-md-1">
            <input name="submit" type="submit" value="szukaj" class="btn btn-primary btn-sm w-100" />
        </div>

        <div class="col-md-1">
            <input name="pdf_create" type="submit" value="pdf" class="btn btn-primary btn-sm w-100" />
        </div>    
        
        <div class="col-md-1">
            <a href="/admin/categories" class="btn btn-primary btn-sm w-100">Wyczyść</a>
        </div>
    </div>

    <div class="card-body form-group">
        <div class="form-row">
            <div class="col-md-4">
                <label for="category_name">Nazwa</label>
                <input id="category_name" name="name" type="text" value="{{Request::get('name')}}" placeholder="Nazwa kategorii" class="form-control"/>
            </div>
            
            <div class="col-md-4">                
                <label for="createdby_name">Utworzył</label>
                <select id="createdby_name" name="createdby" class="form-control">
                    <option value="0" @if(empty(Request::get('createBy'))) selected="selected" @endif>Wszyscy</option>
                    @foreach($authors as $author)
                        <option value="{{$author->id}}" @if($author->id == Request::get('createdby')) selected="selected" @endif>{{$author->name}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="col-md-4">
                <label for="category_active">Aktywny</label>
                <select id="category_active" name="active" class="form-control">
                    
                    <option value="" @if(Request::get('active') == null) selected="selected" @endif>Brak</option>
                        
                    @foreach(Config::get('global.active') as $value => $name)
                        <option value="{{$value}}" @if(Request::get('active') !== null && $value == Request::get('active')) selected="selected" @endif>{{$name}}</option>
                    @endforeach
                    
                </select>
            </div>
        </div>
    </div>

    <div class="card-header">Lista kategorii</div>

    <div class="card-body">

        {{$categories->links()}}
        <table class="table-responsive table table-sm table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="col-sm-2">nazwa
                        <a href="{{CategoriesFormLink::getLink(['sort_name' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{CategoriesFormLink::getLink(['sort_name' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">data utworzenia
                        <a href="{{CategoriesFormLink::getLink(['sort_create_date' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{CategoriesFormLink::getLink(['sort_create_date' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">utworzył
                        <a href="{{CategoriesFormLink::getLink(['sort_created_by' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{CategoriesFormLink::getLink(['sort_created_by' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">data edycji
                        <a href="{{CategoriesFormLink::getLink(['sort_update_date' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{CategoriesFormLink::getLink(['sort_update_date' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">edytował
                        <a href="{{CategoriesFormLink::getLink(['sort_updated_by' => '1'])}}"><img src="{{url('/')}}/img/arrow_up.png" style="float:left; width:15px;" /></a>
                        <a href="{{CategoriesFormLink::getLink(['sort_updated_by' => '2'])}}"><img src="{{url('/')}}/img/arrow_down.png" style="float:left; width:15px;" /></a>
                    </th>
                    <th class="col-sm-2">aktywne</th>
                </tr>
            </thead>
        @foreach($categories as $category)

        <tr>
            <td><a href="/admin/category/{{$category->id}}">{{$category->name}}</a></td>
            <td>{{$category->created_at}}</td>
            <td class="text-center">{{$category->createdBy->name}}</td>
            <td>{{$category->updated_at}}</td>
            <td class="text-center">
                @if($category->updatedBy != null)
                    {{$category->updatedBy->name}}
                @else
                        brak
                @endif
            </td>
            <td class="text-center">
                @if($category->active)
                    <a href="/admin/category/{{$category->id}}/access/0" onclick="return confirm('Czy na pewno dezaktywować?');">tak</a>
                @else
                    <a href="/admin/category/{{$category->id}}/access/1" onclick="return confirm('Czy na pewno aktywować?');">nie</a>
                @endif
            </td>
        </tr>

        @endforeach
        </table>
         {{$categories->appends(CategoriesFormLink::getParamsFromQuery())->links()}}

        <p class="text-right"><a href="/admin/category">Dodaj kategorię &raquo;</a></p>
    </div>

        {{CategoriesFormLink::getInputFromQuery()}}
    </form>
</div>

@endsection