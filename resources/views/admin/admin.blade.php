@extends('layouts.app')

@section('menu')

   <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
           menu<span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a href="/admin/home" class="dropdown-item">Panel</a><br/>
                <a href="/home/account" class="dropdown-item">Edytuj swoje dane</a><br/>
                <a href="/home/password" class="dropdown-item">Zmień swoje hasło</a><br/><br/>
                @if(auth()->user()->is_admin)
                <a href="/admin/users" class="dropdown-item">Lista użytkowników</a><br/>
                <a href="/admin/categories" class="dropdown-item">Lista kategorii</a><br/>
                <a href="/admin/statements" class="dropdown-item">Lista stron</a><br/>
                <a href="/admin/refresh" class="dropdown-item">Zresetuj bazę</a><br/>
                @endif
        </div>
    </li>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
 


        <div class="col-md-12">

            <div class="card">
                @yield('main')
            </div>

        </div>
        
    </div>
</div>

@endsection