@extends('layouts.guest')

@section('content')  
    <div class="content">
        @if($page->page != 'index' && $page->category->name != 'Brak')
        <div class="row">
            <div class="col-12">
                <h3>{{$page->title}}</h3>
                Kategoria: {{$page->category->name}}
            </div>       
        </div>
        @endif
        
        <div class="row pt-4">
            <div class="col-12">
                {!!$page->description!!}
            </div>
        </div>
        

<div class="row pl-4 pr-4 pt-4 signature">
    <div class="text-right col-12 @if($page->updated_at != null && $page->updatedBy != null) col-md-8 @else col-md-12 @endif">        
        Data publikacji: {{$page->created_at}}<br/>
        Opublikował: {{$page->createdBy->first_name}} {{$page->createdBy->surname}}<br/>
    </div>
    
    
    @if($page->updated_at != null && $page->updated_by_users_id != null && $page->updatedBy != null)
    <div class="text-right col-md-4 col-12">
    Data aktualizacji: {{$page->updated_at}}<br/>
    Zaktualizował: {{$page->updatedBy->first_name}} {{$page->updatedBy->surname}}<br/>
    </div>
    @endif
                     
</div>
        
    </div>
@endsection