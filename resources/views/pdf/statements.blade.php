<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BIP ZJO') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <?php /*
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     
     * 
     * linking to external CSS - you're better off writing your css between style tags in the same file as your html

        using blade templating syntax e.g. @sections @roles, etc

        linking to external images

        Complex table structures/layouts 
     */
    ?>
    <style>
        body {
                font-family: DejaVu Sans;
                font-size: 12px;
            }
            
            table td {
                
                text-align: center;
            }
    </style>
    
</head>
<body>
<div id="app">
    <main>
        
        
        <div class="card-header">Lista stron</div>

        <div class="card-body">
            <table class="table table-striped table-bordered table-hover">
                <thead class="">
                    <tr>
                        <th scope="col">tytuł</th><th scope="col">data dodania</th>
                    </tr>
                </thead>
                @foreach($statements as $stm)
                    <tr>
                        <td>{{$stm->title}}</td>
                        <td>{{$stm->created_at}}</td>                
                    </tr>
                @endforeach
            </table>
        </div>
    </main>
</div>
</body>
</html>