@extends('layouts.guest')

@section('content')  
    <div>
        <div class="row">
            <div class="col-12">
                <h3>Wejścia na strone</h3>
            </div>
        </div>
        
        @if($entries != null)
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tr class="row">
                        <th>Liczba odwiedzin na podstronach</th>
                    </tr>
                    @foreach($entries as $entry)
                        <tr class="row">
                            <td class="col-6">{{$entry->title}}</td>
                            <td class="col-6">
                                <div class="text-center bar-size" style="background-color:#E9ECEF; width: {{($entry->amount/$sum) * 20}}rem; height: 100%;">{{$entry->amount}}</div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        @endif
        
        @if($days != null)
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tr class="row">
                        <th>Rozkład tygodniowy</th>
                    </tr>
                    @foreach($days as $day)
                        <tr class="row">
                            <td class="col-sm-5">
                                @switch($day->day)
                                    @case(1)
                                        Niedziela
                                    @break
                                    
                                    @case(2)
                                        Poniedziałek
                                    @break
                                    
                                    @case(3)
                                        Wtorek
                                    @break
                                    
                                    @case(4)
                                        Środa
                                    @break
                                    
                                    @case(5)
                                        Czwartek
                                    @break
                                    
                                    @case(6)
                                        Piątek
                                    @break
                                    
                                    @case(7)
                                        Sobota
                                    @break
                                @endswitch
                                
                            </td>
                            <td class="col-sm-1">{{$day->amount}}</td>
                            <td class="col-sm-6">
                                <div style="background-color:#E9ECEF; width: {{($day->amount/$sum) * 20}}rem; height: 100%;">&nbsp;</div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        @endif
        
        @if($years != null)
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <tr class="row">
                        <th>Liczba wejść na stronę wg. roku</th>
                    </tr>
                    @foreach($years as $year)
                        <tr class="row">
                            <td class="col-sm-5">{{$year->year}}</td>
                            <td class="col-sm-1">{{$year->amount}}</td>
                            <td class="col-sm-6">
                                <div style="background-color:#E9ECEF; width: {{($year->amount/$sum) * 20}}rem; height: 100%;">&nbsp;</div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        @endif
    </div>
@endsection