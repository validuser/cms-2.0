@extends('admin.admin')

@section('main')

            <div class="card">
                <div class="card-header">Zmiana danych</div>

                <div class="card-body">

                    
                    <form method="POST" action="/home/save_password" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="nick">Nick</label>
                            <input id="nick" type="text" value="{{$user->name}}" class="form-control" disabled="disabled" />
                        </div>
                                         
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" value="{{$user->email}}" class="form-control" disabled="disabled" />
                        </div>
                        
                        <div class="form-group">
                            <label for="new_password">Nowe Hasło</label>
                            <input type="password" autocomplete="off" id="new_password" value="" placeholder="Nowe hasło" name="new_password" class="form-control @error('password') is_invalid @enderror"  />
                        </div>
                        
                        <div class="form-group">
                            <label for="repeat_new_password">Powtórz nowe hasło</label>
                            <input type="password" autocomplete="off" id="repeat_new_password" value="" placeholder="Powtórz nowe hasło" name="repeat_new_password" class="form-control @error('repeat_new_password') is_invalid @enderror" />
                        </div>
                        
                        <div class="form-group">
                            <label for="old_password">Stare hasło</label>
                            <input type="password" autocomplete="off" id="old_password" value="" placeholder="Stare hasło" name="old_password" class="form-control @error('old_password') is_invalid @enderror" />
                        </div>
                        
 
                        <input name="save" type="submit" value="Zapisz" class="form-control btn-primary" />


                        @csrf
                    </form>
                </div> 
            </div>

@endsection
