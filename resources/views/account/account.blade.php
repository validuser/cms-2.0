@extends('admin.admin')

@section('main')

            <div class="card">
                <div class="card-header">Zmiana danych</div>

                <div class="card-body">

                    
                    <form method="POST" action="/home/save_account" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="nick">Nick</label>
                            <input id="nick" type="text" value="{{$user->name}}" class="form-control" disabled="disabled" />
                        </div>
                        
                        <div class="form-group">
                            <label for="first_name">Imię:</label>
                            <input id="first_name" name="first_name" type="text" value="{{$user->first_name}}" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="surname">Nazwisko</label>
                            <input id="surname" name="surname" type="text" value="{{$user->surname}}" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="personal_identity_number">Pesel</label>
                            <input id="personal_identity_number" name="personal_identity_number" type="number" value="{{$user->personal_identity_number}}" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                            <label for="department">Dział</label>
                            <input id="department" name="department" type="text" value="{{$user->department}}" class="form-control" />   
                        </div>
                        
                        <div class="form-group">
                            <label for="phone">Telefon</label>
                            <input id="phone" name="phone" type="number" value="{{$user->phone}}" class="form-control" />
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" value="{{$user->email}}" class="form-control" disabled="disabled" />
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="avatar">Avatar</label>
                            
                            <input type="file" id="avatar" name="avatar" class="form-control" />
                            
                            <img src="{{$avatar}}" />
                        </div>
                        <input name="save" type="submit" value="Zapisz" class="form-control btn-primary" />
                        
                        
                        <input type="hidden" name="id" value="{{$user->id}}" />

                        @csrf
                    </form>
                </div> 
            </div>

@endsection
