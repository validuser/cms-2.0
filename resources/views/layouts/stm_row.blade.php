
<div class="row pl-4 pr-4 pt-4">
    <div class="col-12 text-left">
      <u><a href="{{$stm->link()}}">{{$stm->title}}</a></u>     
    </div>
             
</div>

<div class="row mt-4 p-4 ">
    <div class="col-12 text-left">
     {{$stm->shortDescription()}}
    </div>
</div>

<div class="row pb-4 signature border-bottom">
    <div class="text-right col-12 @if($stm->updated_at != null && $stm->updatedBy != null) col-md-8 @else col-md-12 @endif">        
        Data publikacji: {{$stm->created_at}}<br/>
        Opublikował: {{$stm->createdBy->first_name}} {{$stm->createdBy->surname}}<br/>
    </div>
    
    
    @if($stm->updated_at != null && $stm->updatedBy != null)
    <div class="text-right col-md-4 col-12">
    Data aktualizacji: {{$stm->updated_at}}<br/>
    Zaktualizował: {{$stm->updatedBy->first_name}} {{$stm->updatedBy->surname}}<br/>
    </div>
    @endif
                     
</div>
