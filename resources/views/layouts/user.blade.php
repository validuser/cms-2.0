@extends('layouts.app')

@section('menu')

   <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
           menu<span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a href="/home" class="dropdown-item">Panel</a><br/>
                <a href="/home/account" class="dropdown-item">Edytuj swoje dane</a><br/>
                <a href="/home/password" class="dropdown-item">Zmień swoje hasło</a><br/><br/>
        </div>
    </li>
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">
 

        <div class="col-md-12">

            <div class="card">
                @yield('main')
            </div>

        </div>
        
    </div>
</div>

@endsection
