<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@if(!empty($page) && !empty($page->head_title)) {{$page->head_title}} @elseif(!empty($title)) {{$title}} @else  Strona główna @endif</title>
        <meta name="description" content="@if(!empty($page) && !empty($page->meta_description)) {{$page->meta_description}} @elseif(!empty($meta_description)) {{$meta_description}} @else Opis dla strony @endif" />
        <meta name="keywords" content="@if(!empty($page) && !empty($page->meta_keywords)) {{$page->meta_keywords}} @elseif(!empty($meta_keywords)) {{$meta_keywords}} @else Słowa kluczowe @endif" />
        
        <!-- Styles -->
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>


<body @if(!empty(Cookie::get('font_size')))id="fs{{Cookie::get('font_size')}}"@endif>
 <div class="wrapper">
            <nav id="sidebar">
            <div class="sidebar-header">
                <a href="{{url('/')}}"><img src="{{asset('img/2.png')}}" alt="Logo" class="mt-1 ml-5" /></a>        
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    @foreach($categories as $category)
                    
                        <a href="#Submenu{{$category->id}}" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">{{$category->name}}</a>
                                            
                        @if($category->statements) 
                            <ul class="collapse show list-unstyled" id="Submenu{{$category->id}}">
                        
                            @foreach($category->statements as $stm)
                                 <li>
                                     <a href="{{$stm->link()}}">{{$stm->title}}</a>
                                </li>
                            @endforeach
                        
                            </ul>
                        @endif
                        
                    @endforeach
                                      
                </li>                
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light">
                

                
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>
                    <button class="btn  d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    
                    <!-- btn-dark <img src="img/zjobip.png" style=" margin-left:20px; width:165px;" /><br/><br/>-->
                    


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">

                            <!--
                            <li class="nav-item">
                                <span class="nav-link">Zmień język
                                <a href="/zmien_jezyk/pl"><img src="{{asset('img/polish20x20.png')}}" alt="flaga Polski"/></a>
                                <a href="/zmien_jezyk/en"><img src="{{asset('img/english20x20.png')}}" alt="flaga Polski"/></a>
                                </span>
                            </li>
                            -->
                            <li class="nav-item">
                                <span class="nav-link">Dla słabowidzących 
                                <a href="/font_size/1" class="short-vision-1">A</a>
                                <a href="/font_size/2" class=" short-vision-2">A</a>
                                <a href="/font_size/3" class=" short-vision-3">A</a>
                                </span>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/statystyki">Statystyki</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/redakcja">Redakcja</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/instrukcja_obslugi">Instrukcja obsługi</a>
                            </li>
                            
                            <li class="nav-item">
                                <form method="Get" action="/search" class="nav-link">
                                Szukaj <input name="text" type="text" value="" style="width:80px; height:20px;"/>
                                       <input class="btn" type="submit" value="&raquo;" style="height:20px; width:22px; padding:0; margin:0;" />
                                       @csrf
                                </form>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="/login">Zaloguj się</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            
            @if($errors->count)
                <div class="row justify-content-center">

                    @foreach ($errors->all() as $error)

                        <div class="alert alert-danger"><?php echo str_replace(['surname', 'first name', 'personal identity number', 'phone'], ['nazwisko', 'imie', 'pesel', 'telefon'], $error); ?></div>

                    @endforeach

                </div>
            @endif
            
            @if(!empty($page) && $page->page == 'index')
                <div class="news">
                    <div class="row uppercase">       
                        <div class="col-12" style="font-size:1.1rem;">Aktualności:</div>
                    </div>

                    <div class="row">
                         @foreach($news as $n)
                             <div class="col-md-4 col-12">
                                 <u><a href="{{$n->link()}}">{{$n->title}}</a></u><br/>
                                 Data publikacji: {{$n->created_at}}<br/>
                                 Kategoria: {{$n->category->name}}<br/>
                             </div>
                         @endforeach
                     </div>

                     <div class="row text-right">
                         <div class="col-12"><a href="{{url('/statements')}}">Pokaż więcej &raquo;</a></div>
                     </div>
                </div>            
            @endif
            
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a @if(empty($page) && empty($categoryBreadCrumbs)) aria-current="page" @endif href="{{url('/')}}">Strona główna</a></li>
                  
                  @if(!empty($categoryBreadCrumbs))
                    <li @if(empty($page)) class="breadcrumb-item active" @else class="breadcrumb-item" @endif>
                        @if(empty($page)) 
                          {{$categoryBreadCrumbs->name}}
                        @else
                          <a href="{{url('/')}}/{{$categoryBreadCrumbs->link}}">{{$categoryBreadCrumbs->name}}</a>
                        @endif                      
                    </li>
                  @endif
                  
                  @if(!empty($page) && $page->page != 'index')
                    <li class="breadcrumb-item active" aria-current="page">{{$page->title}}</li>
                  @endif
                  
                  @if(!empty($bread))
                    <li class="breadcrumb-item active" aria-current="page">{{$bread}}</li>
                  @endif
                </ol>
            </nav>
            
            @if(!empty($page) && !empty($page->tts_audio_path))
            <div class="text-right">                
                 <audio controls id="audio">
                    <source src="{{asset('storage/tts/'.$page->tts_audio_path)}}" type="audio/mpeg">
                    Twoja przegadarka nie obsługuje odtwarzania plików dźwiękowych
                </audio>
            </div>
            @endif
            
            @yield('content')
            
</div> <!-- page content -->

 </div>
<footer class="page-footer mt-3">
    <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-12">
            <h6 class="text-uppercase font-weight-bold">Dodatkowe informacje</h6>
            <p>Bardzo ważne informacje</p>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12">
            <h6 class="text-uppercase font-weight-bold">Dane kontaktowe</h6>
            <p>Dolina Krzemowa 13, 09-410 Płock
            <br/>informacja@domena.pl
            <br/>- 555 444 333
            <br/>- 24 2648 568</p>
          </div>
        </div>
        <div class="footer-copyright text-center">© 2020 Copyright: Strona testowa</div>
    </div>
</footer>  
        
        @if(empty(Cookie::get('cookies_permission')))    
        <div id="cookies" class="row">
            <div class="col-12 text-center">
                Strona do poprawnego działania wykorzystuje pliki <u><a href="/cookies" target="_blank">cookies</a></u>. <a href="/cookies_confirm" class="btn btn-primary">Zgadzam sie</a>
            </div>
        </div>
        @endif
    
        <!-- Scripts -->
            <script src="{{ asset('js/app.js') }}" defer></script>
            <script src="{{ asset('js/main.js') }}" defer></script>
        
        <script src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"  crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"  crossorigin="anonymous"></script>
        
    </body>
</html>