@extends('layouts.guest')

@section('content')  
    <div>
        <div class="row">
            <div class="col-12">
                <h3>{{$category->name}}</h3>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                @foreach($category->statements as $stm)
                    @include('layouts.stm_row')
                @endforeach
            </div>
        </div>
    </div>
@endsection