@extends('layouts.guest')

@section('content')

    <div class="text-center">
        @if(!empty($search))
        <div class="row text-left">
            <div class="col-12 pl-4" style="font-size:1.1rem;">
                Wyszukiwanie dla: <i>"{{$search}}"</i>
            </div>
        </div>
        @endif
        
        @if($statements->count() > 0)
        
            @foreach($statements as $stm)

                @include('layouts.stm_row')

            @endforeach   
        @else
        <i>Brak wyników dla danego wyszukiwania!</i>
        @endif
    </div>

    <div class="mt-4 row text-center">
        <div class="col-12">
            {{$statements->links()}}
        </div>
    </div>
@endsection