<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (config('app.env') === 'production') {
    URL::forceScheme('https');
}

Auth::routes();

Route::get('/test', 'TextToSpeechController@index');
Route::get('/text-to-speech-convert', 'TextToSpeechController@TextToSpeechConvert')->name('text-to-speech-convert');

Route::get('/zmien_jezyk/{language}', 'UserController@changeLanguage');

# Route::get('/', 'UserController@index');
Route::get('/cookies_confirm', 'UserController@setCookie');
Route::get('/font_size/{size}', 'UserController@setCookieWithFontSize');
# Route::get('/category-{categoryId}/page-{pageId}', 'UserController@getPage');
Route::get('/statements', 'UserController@getStatements');
Route::get('/search', 'UserController@getSearch');
Route::get('/statystyki', 'UserController@getMostViewed');

Route::get('/home', 'UserAccountController@index')->name('home'); # redirect()->route('home');
Route::get('/home/account', 'UserAccountController@account');
Route::post('/home/save_account', 'UserAccountController@saveAccount');

Route::get('/home/password', 'UserAccountController@password');
Route::post('/home/save_password', 'UserAccountController@savePassword');

#->middleware('is_admin'); == construct() $this->middleware('is_admin')

Route::get('/admin/text', 'AdminController@textToSpeech'); # ->middleware('is_admin');
Route::get('/admin', 'AdminController@index')->name('admin.home'); # ->middleware('is_admin');
Route::get('/admin/access/{id}/{access?}', 'AdminController@setAdminAccess')->name('admin.access'); #->middleware('is_admin');

Route::get('/admin/users', 'AdminController@adminUsers')->name('admin.users'); #->middleware('is_admin');
Route::get('/admin/user/{id}', 'AdminController@account')->name('admin.user'); #->middleware('is_admin');

Route::get('/admin/user/password/{id}', 'AdminController@changePassword'); #->middleware('is_admin');

Route::get('/admin/user/active/{id}/{option?}', 'AdminController@setActive'); #->middleware('is_admin');

Route::get('/admin/categories', 'AdminController@getCategories'); #->middleware('is_admin');
Route::get('/admin/category/{id?}', 'AdminController@getCategory'); #->middleware('is_admin');
Route::post('/admin/category/save', 'AdminController@saveCategory'); #->middleware('is_admin');

Route::get('/admin/category/{id}/access/{option}', 'AdminController@setActiveCategory'); #->middleware('is_admin');

Route::get('/admin/statements', 'AdminController@getStatements'); #->middleware('is_admin');
Route::get('/admin/statement/{id?}', 'AdminController@getStatement'); #->middleware('is_admin');
Route::post('/admin/statement/save', 'AdminController@saveStatement'); # ->middleware('is_admin');

Route::get('/admin/statement/{id}/access/{option}', 'AdminController@setActiveStatement'); #->middleware('is_admin');

Route::get('admin/ttt', 'AdminController@getSpeechFromText');

Route::get('admin/refresh', 'AdminController@reset');
Route::get('admin/schema/{name}', 'AdminController@getSchema');
Route::get('admin/tables', 'AdminController@getTables');


Route::get('/{name?}/{name2?}', 'UserController@getPageByName');

Route::get('image', 'ImageController@index');
Route::post('save-image', 'ImageController@save');
